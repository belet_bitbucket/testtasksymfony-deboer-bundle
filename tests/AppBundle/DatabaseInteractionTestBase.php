<?php

/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 21.07.2016
 * Time: 19:00
 */
namespace Tests\AppBundle;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class DatabaseInteractionTestBase extends WebTestCase
{
    /** @var  Application */
    protected $application;

    /**
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    public function getContainer()
    {
        return $this->application->getKernel()->getContainer();
    }

    /**
     * SettingUp test database before each test
     */
    public function setUp()
    {
        $kernel = new \AppKernel("test", true);
        $kernel->boot();
        $this->application = new Application($kernel);
        $this->application->setAutoExit(false);
        $this->runConsole("doctrine:schema:drop", array("--force" => true));
        $this->runConsole("doctrine:schema:create");
    }

    /**
     * Execute console command
     * @param $command
     * @param array $options
     * @return int
     */
    protected function runConsole($command, $options = array())
    {
        $options["-e"] = "test";
        $options["-n"] = true;
        $options = array_merge($options, array('command' => $command));
        return $this->application->run(new \Symfony\Component\Console\Input\ArrayInput($options));
    }

    /**
     * Fill up database from fixtures.
     * @param $fixtures
     */
    protected function fillDatabaseFromFile($fixtures)
    {
        $this->runConsole("doctrine:fixtures:load", array("--fixtures" => $fixtures));
        //$this->runConsole("doctrine:fixtures:load", array("--fixtures" => __DIR__ . "/../DataFixtures"));
    }
}

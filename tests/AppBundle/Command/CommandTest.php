<?php

/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 18.07.2016
 * Time: 13:01
 */

namespace Tests\AppBundle\Command;

use Symfony\Component\Console\Tester\CommandTester;
use Tests\AppBundle\DatabaseInteractionTestBase;

class Command extends DatabaseInteractionTestBase
{
    /**
     * Wrong file name test.
     */
    public function testWrongFileName()
    {

        $filename = 'sock.csv';
        $this->execute(
            array(
                'filename' => $filename,
                '--test' => false
            ),
            0,
            '',
            'No such file or directory'
        );
    }

    /**
     * Dir instead file test.
     */
    public function testOnDirName()
    {

        $filename = 'tests';
        $this->execute(
            array(
                'filename' => $filename,
                '--test' => false
            ),
            0,
            '',
            'Cannot use SplFileObject with directories'
        );
    }

    /**
     * Normal import test.
     */
    public function testImport()
    {

        $filename = 'stock.csv';
        $this->execute(
            array(
                'filename' => $filename,
                '--test' => false
            ),
            23,
            '[OK] Import finished'
        );
    }

    /**
     * Execute appBundle:database:import command.
     *
     * @param $params
     * @param $assertDatabaseProductsNum
     * @param $assertMessage
     * @param string $assertEx
     */
    private function execute($params, $assertDatabaseProductsNum, $assertMessage, $assertEx = '')
    {
        $this->application->add(new \AppBundle\Command\ImportDataFromFileCommand());

        $command =  $this->application->find('appBundle:database:import');
        $commandTester = new CommandTester($command);
        try {
            $commandTester->execute($params);
            $this->assertContains($assertMessage, $commandTester->getDisplay());
            $productRepository = $this->getContainer()
                ->get('doctrine.orm.entity_manager')
                ->getRepository('AppBundle:Product');
            $this->assertEquals($assertDatabaseProductsNum, count($productRepository->findAll()));
        } catch (\Exception $e) {
            $this->assertContains($assertEx, $e->getMessage());
        }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 22.07.2016
 * Time: 11:12
 */

namespace Tests\AppBundle\Controller;

use Tests\AppBundle\DatabaseInteractionTestBase;

class ProductsControllerTest extends DatabaseInteractionTestBase
{
    /** @var string  */
    private $productFixturePath = 'tests/AppBundle/Fixtures/LoadProductData.php';

    /**
     * Get all products api test.
     */
    public function testGetAllAction()
    {
        $client = static::createClient();
        $this->fillDatabaseFromFile($this->productFixturePath);
        $client->request('GET', '/api/v1/products');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonFile('ProductDataJsonResponseAll.json', $client->getResponse()->getContent());
    }

    /**
     * Get product by id api test.
     */
    public function testGetProductAction()
    {
        $client = static::createClient();
        $this->fillDatabaseFromFile($this->productFixturePath);
        $client->request('GET', '/api/v1/products/1');

        $file = file('ProductDataJsonResponseAll.json');
        $allProducts = json_decode($file[0]);
        $expectedProductJson = json_encode($allProducts[0]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJsonStringEqualsJsonString($expectedProductJson, $client->getResponse()->getContent());
    }

    /**
     * Get product by id api test with incorrect id.
     */
    public function testGetProductWithIncorrectIdAction()
    {
        $client = static::createClient();
        $this->fillDatabaseFromFile($this->productFixturePath);
        $client->request('GET', '/api/v1/products/100');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    /**
     * New Product test.
     */
    public function testPostProductAction()
    {
        $product = json_encode(array(
            'str_product_name' => 'Product3',
            'str_product_desc' => 'Third',
            'str_product_code' => 'P0003',
            'dtm_discontinued' => null,
            'int_stock' => 3,
            'float_price' => 3.3
        ));
        $this->fillDatabaseFromFile($this->productFixturePath);
        $client = static::createClient(array(
            'request.options' => array(
                'exceptions' => false,
            )));

        $client->request(
            'POST',
            '/api/v1/products',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json'
            ],
            $product
        );
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        $this->assertEquals('api/v1/products/3', $client->getResponse()->headers->get('location'));
    }

    /**
     * Patch Product test.
     */
    public function testPatchProductAction()
    {
        $productWithNewName = json_encode(array(
            'str_product_name' => 'Product1.1',
            'str_product_desc' => 'First',
            'str_product_code' => 'P0001',
            'dtm_discontinued' => null,
            'int_stock' => 1,
            'float_price' => 1
        ));
        $this->fillDatabaseFromFile($this->productFixturePath);
        $client = static::createClient(array(
            'request.options' => array(
                'exceptions' => false,
            )));

        $client->request(
            'PUT',
            '/api/v1/products/1',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json'
            ],
            $productWithNewName
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('api/v1/products/1', $client->getResponse()->headers->get('location'));
        $patchedEntity = $this->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Product')
            ->findOneById(1);
        $this->assertEquals('Product1.1', $patchedEntity->getStrProductName());
    }

    /**
     * Delete product by id api test.
     */
    public function testDeleteProductAction()
    {
        $client = static::createClient();
        $this->fillDatabaseFromFile($this->productFixturePath);
        $client->request('DELETE', '/api/v1/products/1');
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        $deleted = $this->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('AppBundle:Product')
            ->findOneById(1);
        $this->assertEquals(null, $deleted);
        $client->request('DELETE', '/api/v1/products/1');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}

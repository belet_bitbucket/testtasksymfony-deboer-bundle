<?php

/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 20.07.2016
 * Time: 13:33
 */
namespace Tests\AppBundle\Classes;

use AppBundle\Classes\ImportHelper\ImportDataHelper;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Validator\Validator\RecursiveValidator;

class ImportHelperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * test ImportDataHelper in normal mode on test file.
     */
    public function testImportOnTestFile()
    {
        $this->execute('testFile.csv', false, 1);
    }

    /**
     * Test ImportDataHelper on file with incorrect filename/
     */
    public function testImportOnNoFile()
    {
        $this->execute('testfile.csv', false, 1, 'No such file or directory');
    }

    /**
     * Test is testmode call rollback
     */
    public function testImportInTestMode()
    {
        $this->execute('testFile.csv', true, 1);
    }

    /**
     * Execute ImportDataHelper tests with its params
     * @param $filename
     * @param $testMode
     * @param int $persistNum
     * @param string $exceptionMessage
     */
    private function execute($filename, $testMode, $persistNum = 0, $exceptionMessage = '')
    {
        $entityManagerMock = $this->getEntityManager();
        $validatorMock = $this
            ->getMockBuilder(RecursiveValidator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $loggerMock = $this
            ->getMockBuilder(ConsoleLogger::class)
            ->disableOriginalConstructor()
            ->getMock();

        $importHelper = new ImportDataHelper($entityManagerMock, $validatorMock);

        $entityManagerMock
            ->expects($this->once())
            ->method('beginTransaction');
        $entityManagerMock
            ->expects($this->once())
            ->method($testMode ? 'rollback' : 'commit');
        $entityManagerMock
            ->expects($this->exactly($persistNum))
            ->method('persist');
        try {
            $importHelper->import($filename, $testMode, $loggerMock);
        } catch (Exception $e) {
            $this->assertContains($exceptionMessage, $e->getMessage());
        }
    }

    /**
     * Returns entity manager Mock
     * @return EntityManager|\PHPUnit_Framework_MockObject_MockObject
     */
    private function getEntityManager()
    {
        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')
            ->setMethods(array('getRepository', 'getClassMetadata', 'persist', 'flush', 'clear', 'getConnection', 'getReference','beginTransaction', 'commit','rollback'))
            ->disableOriginalConstructor()
            ->getMock();

        $repo = $this->getMockBuilder('Doctrine\ORM\EntityRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $metadata = $this->getMockBuilder('Doctrine\ORM\Mapping\ClassMetadata')
            ->setMethods(array('getName', 'getFieldNames', 'getAssociationNames', 'setFieldValue', 'getAssociationMappings'))
            ->disableOriginalConstructor()
            ->getMock();

        $metadata->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('Ddeboer\DataImport\Tests\Fixtures\Entity\TestEntity'));

        $metadata->expects($this->any())
            ->method('getFieldNames')
            ->will($this->returnValue(array('firstProperty', 'secondProperty')));

        $metadata->expects($this->any())
            ->method('getAssociationNames')
            ->will($this->returnValue(array('firstAssociation')));

        $metadata->expects($this->any())
            ->method('getAssociationMappings')
            ->will($this->returnValue(array(array('fieldName' => 'firstAssociation','targetEntity' => 'Ddeboer\DataImport\Tests\Fixtures\Entity\TestEntity'))));

        $configuration = $this->getMockBuilder('Doctrine\DBAL\Configuration')
            ->setMethods(array('getConnection'))
            ->disableOriginalConstructor()
            ->getMock();

        $connection = $this->getMockBuilder('Doctrine\DBAL\Connection')
            ->setMethods(array('getConfiguration', 'getDatabasePlatform', 'getTruncateTableSQL', 'executeQuery'))
            ->disableOriginalConstructor()
            ->getMock();

        $connection->expects($this->any())
            ->method('getConfiguration')
            ->will($this->returnValue($configuration));

        $connection->expects($this->any())
            ->method('getDatabasePlatform')
            ->will($this->returnSelf());

        $connection->expects($this->any())
            ->method('getTruncateTableSQL')
            ->will($this->returnValue('TRUNCATE SQL'));

        $connection->expects($this->any())
            ->method('executeQuery')
            ->with('TRUNCATE SQL');

        $em->expects($this->once())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $em->expects($this->once())
            ->method('getClassMetadata')
            ->will($this->returnValue($metadata));

        $em->expects($this->any())
            ->method('getConnection')
            ->will($this->returnValue($connection));

        return $em;
    }
}

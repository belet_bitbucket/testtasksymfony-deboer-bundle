<?php

/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 22.07.2016
 * Time: 11:20
 */
namespace Tests\AppBundle\Fixtures;

use AppBundle\Entity\Product;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadProductData implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $products = [
            ['Product1','P0001' ,'First', 1, 1.0, ''],
            ['Product2','P0002' ,'Second', 2, 2.0, '']
        ];
        foreach ($products as $product) {
            $productEntity = new Product();
            $productEntity->setStrProductName($product[0]);
            $productEntity->setStrProductCode($product[1]);
            $productEntity->setStrProductDesc($product[2]);
            $productEntity->setIntStock($product[3]);
            $productEntity->setFloatPrice($product[4]);
            if ($product[5]) {
                $productEntity->setDtmDiscontinued(new \DateTime());
            }
            $manager->persist($productEntity);
        }
        $manager->flush();
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 21.07.2016
 * Time: 16:36
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Type\ProductType;
use FOS\RestBundle\Controller\Annotations\View as ViewAnnotation;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ProductsController extends FOSRestController
{
    /**
     * @ApiDoc(description="Return a product found by Id",
     *     statusCodes={
     *         200="Returned when successful",
     *         404="Returned when the product is not found"
     *         },
     *     section="products"
     *      )
     * @param  integer $id product Id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getProductAction($id)
    {
        $product = $this->getDoctrine()->getRepository('AppBundle:Product')->find($id);

        if (!$product instanceof Product) {
            throw new NotFoundHttpException('Product not found');
        }

        $view = $this->view($product, 200);
        $handledView = $this->handleView($view);
        return $handledView;
    }

    /**
     * @ApiDoc(description="Return a products according to selector",
     *     statusCodes={
     *         200="Returned when successful"
     *         },
     *     section="products"
     *      )
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getProductsAction(Request $request)
    {
        $productRepository = $this->getDoctrine()->getRepository('AppBundle:Product');
        $selector = json_decode($request->query->get('selector'));
        $count_only = $request->query->get('count_only');
        $result = null;
        if ($selector === null) {
            $result = $productRepository->getProducts();
        } else {
            $propertiesDeserializer = $this->get('app.deserializer.request_properties');
            $filters = null;
            foreach ($selector->filters as $filter) {
                $filters[] = $propertiesDeserializer->deserializeFilter($filter);
            }
            $order = $propertiesDeserializer->deserializeOrder($selector->order);
            $result = $productRepository->getProducts(
                $selector->offset,
                $selector->maxResults,
                $order,
                $filters,
                $count_only
            );
        }
        if ($count_only) {
            $result = $result[0][1];
        }
        $view = $this->view($result, 200);
        return $this->handleView($view);
    }

    /**
     * @ApiDoc(description="Create new Product if form is valid",
     *     statusCodes={
     *         201="Returned when successful",
     *         400="Returned if form isn't valid."
     *         },
     *     section="products"
     *      )
     * @return Response
     */
    public function postProductAction(Request $request)
    {
        return $this->processForm($request, new Product());
    }

    /**
     * @ApiDoc(description="Update Product if is form valid",
     *     statusCodes={
     *         200="Returned when successful",
     *         400="Returned if form isn't valid.",
     *         404="Returned when the product is not found"
     *         },
     *     section="products"
     *      )
     * @param Request $request
     * @param  integer $id product Id
     * @return Response
     */
    public function putProductAction(Request $request, $id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $product = $em->getRepository('AppBundle:Product')->findOneById($id);
        if ($product instanceof Product) {
            $request->request->remove('id');
            return $this->processForm($request, $product);
        }
        throw new NotFoundHttpException('Product not found');
    }


    /**
     * @ApiDoc(description="Delete product entity",
     *     statusCodes={
     *         204="Returned when successful",
     *         404="Returned when the product is not found"
     *         },
     *     section="products"
     *      )
     * @param  integer $id product Id
     * @return Response
     */
    public function deleteProductAction($id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $product = $em->getRepository('AppBundle:Product')->findOneById($id);
        if ($product instanceof Product) {
            $em->remove($product);
            $em->flush();
            return $this->handleView($this->view(null, 204));
        }
        throw new NotFoundHttpException('Product not found');
    }

    /**
     * Create or update product if form valid
     * @param Request $request
     * @param Product $product
     * @return View|Response
     */
    private function processForm(Request $request, Product $product)
    {
        $statusCode = $product->getId() === null ? 201 : 200;
        $form = $this->createForm(ProductType::class, $product);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($product);
            $em->flush();

            $response = new Response();
            $response->setStatusCode($statusCode);
            $response->headers->set(
                'Location',
                'api/v1/products/'.$product->getId()
            );
            return $response;
        }
        $handledView = $this->handleView($this->view($form->getData(), 400));
        $errors = $this->get('app.serializer.form_errors')->serializeForm($form);
        $handledView->setContent(json_encode($errors));
        return $handledView;
    }
}

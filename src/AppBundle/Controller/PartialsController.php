<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 27.07.2016
 * Time: 13:37
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PartialsController extends Controller
{
    public function givePartialAction($templateName)
    {
        return $this->render('@App/api/'.$templateName.'.twig');
    }
}

/**
 * Created by a2.beletsky on 26.07.2016.
 */
'use strict';

var app = angular.module('importApp', ['ui.router', 'ngResource']);

app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

app.directive('dateInput', function(){
    return {
        restrict : 'A',
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
            ngModelCtrl.$formatters.length = 0;
            ngModelCtrl.$parsers.length = 0;
        }
    }
});
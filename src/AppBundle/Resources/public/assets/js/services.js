/**
 * Created by a2.beletsky on 27.07.2016.
 */
'use strict';

/**
 * Product resource factory.
 */
app.factory('Product', function($resource) {
    return $resource('/api/v1/products/:id', { id:'@id' }, {
        get:    {method: 'GET'},
        query: {method: 'GET', isArray:true},
        update: {method: 'PUT'},
        create: {method: 'POST'},
        delete: {method: 'DELETE'}
    });
});

/**
 * Products state provider
 */
app.config(function($stateProvider) {
    $stateProvider.state('products', { // state for showing all products
        url: '/products',
        templateUrl: 'partials/products.html',
        controller: 'ProductsListController'
    }).state('newProduct', { //state for adding a new product
        url: '/products/new',
        templateUrl: 'partials/product_edit.html',
        controller: 'ProductFormController'
    }).state('editProduct', { //state for updating a product
        url: '/movies/:id/edit',
        templateUrl: 'partials/product_edit.html',
        controller: 'ProductFormController'
    });
}).run(function($state) {
    $state.go('products'); //make a transition to products state when app starts
});

/**
 * Entity selector service
 */
app.service('EntitySelector', function () {
    this.selectPerRequest = 10;
    var selector = {
        'offset' : 0,
        'maxResults' : this.selectPerRequest,
        'order' : {
            'field' : '',
            'direction' : ''
        },
        'filters' : []
    };

    /**
     * @public
     * @returns {{begin: number, end: number, ordered: {by: string, how: string}, filters: Array}}
     */
    this.getSelector = function () {
        return selector;
    };

    /**
     * Return Selector to default params
     * @public
     */
    this.clear = function () {
        selector = {
            'offset' : 0,
            'maxResults' : this.selectPerRequest,
            'ordered' : {
                'by' : '',
                'how' : ''
            },
            'filters' : []
        };
    };

    /**
     * Update selector.begin and selector.end according to page.
     * @param page
     * @public
     */
    this.setPage = function (page) {
        selector.offset = (page - 1) * this.selectPerRequest;
        selector.maxResults = this.selectPerRequest;
    };

    /**
     * Select entities order
     * @param order
     * @public
     */
    this.setOrdered = function(order) {
        selector.order = order;
    };

    /**
     * Add entities filter
     * @param filter
     * @public
     */
    this.addFilter = function (filter) {
        selector.filters.push(filter)
    };

    /**
     * Clear all filters.
     * @public
     */
    this.clearFilters = function () {
        selector.filters = [];
    }
});

/**
 * Pagination service
 */
app.service('PagerService', function() {

    var pager = {
        totalItems: 0,
        currentPage: 1,
        pageSize: 10,
        totalPages: 0,
        startPage: 0,
        endPage: 0,
        pages: 0
    };

    /**
     * Sets Pager params
     * @param totalItems
     * @param pageSize
     * @public
     */
    this.setParams = function(totalItems, pageSize) {
        pager.totalItems = totalItems;
        pager.pageSize = pageSize;
    };

    /**
     * Set current page
     * @param page
     * @public
     */
    this.setPage = function (page) {
        pager.currentPage = page;
    };

    /**
     * @public
     * @returns {{totalItems: number, currentPage: number, pageSize: number, totalPages: number, startPage: number, endPage: number, pages: number}}
     */
    this.getPager = function () {

        // calculate total pages
        pager.totalPages = Math.ceil(pager.totalItems / pager.pageSize);

        if (pager.totalPages <= 10) {
            // less than 10 total pages so show all
            pager.startPage = 1;
            pager.endPage = pager.totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (pager.currentPage <= 6) {
                pager.startPage = 1;
                pager.endPage = 10;
            } else if (pager.currentPage + 4 >= pager.totalPages) {
                pager.startPage = pager.totalPages - 9;
                pager.endPage = pager.totalPages;
            } else {
                pager.startPage = pager.currentPage - 5;
                pager.endPage = pager.currentPage + 4;
            }
        }

        // create an array of pages to ng-repeat in the pager control
        pager.pages = _.range(pager.startPage, pager.endPage + 1);
        return pager;
    }
});
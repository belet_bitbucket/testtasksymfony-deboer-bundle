/**
 * Created by a2.beletsky on 26.07.2016.
 */
app.controller('ProductsListController', function ($scope, $http, EntitySelector, PagerService, Product) {

    /**
     * Make request to get products according to Selector
     * @private
     */
    var getProducts = function () {
        $http.get('/api/v1/products',{params : {selector : EntitySelector.getSelector()}}).success(
            function (response) {
                $scope.products = response;
            }
        )
    };

    /**
     * Make request to get total Products number with tuned selector.
     * @private
     */
    var getProductsCount = function () {
        $http.get('/api/v1/products',{params : {selector : EntitySelector.getSelector(), count_only : true}}).success(
            function (response) {
                PagerService.setParams(parseInt(response), EntitySelector.selectPerRequest);
                $scope.paginator = PagerService.getPager();
            }
        )
    };

    /**
     * Make request to get products of specified page.
     * Fill $scope.products if success.
     * @param page
     * @public
     */
    $scope.getProductsFromPage = function (page) {
        EntitySelector.setPage(page);
        PagerService.setPage(page);
        $scope.paginator = PagerService.getPager();
        getProducts();

    };
    /**
     * Delete Product entity
     * Call getProductsFromPage to update page to preserve the integrity.
     * @param $index
     * @public
     */
    $scope.deleteProduct = function ($index) {
        var confirmMessage = 'Are you sure you want to delete product with code: '
            + $scope.products[$index].str_product_code
            +'?';
        if (confirm(confirmMessage)) {
            Product.delete({id: $scope.products[$index].id},
                function () {
                    getProductsCount();
                    $scope.getProductsFromPage($scope.paginator.currentPage);
                }
            );
        }
    };

    $scope.doOrder = function (field_name) {
        if (field_name !== $scope.order.field) {
            $scope.order.field = field_name;
            $scope.order.direction = 'ASC';
        } else {
            $scope.order.direction = $scope.order.direction === 'ASC' ? 'DESC' : 'ASC';
        }
        EntitySelector.setOrdered($scope.order);
        getProducts();
    };

    /**
     * Initialize controller.
     * @public
     */
    $scope.init = function () {
        $scope.products = [];
        $scope.paginator = {};
        $scope.orderField = '';
        $scope.order = {
            'direction' : '',
            'field' : ''
        };
        $scope.fieldNames = {
            'Code': 'str_product_code',
            'Name': 'str_product_name',
            'Description': 'str_product_desc',
            'Stock': 'int_stock',
            'Cost in GBP': 'float_price',
            'Discontinued': 'dtm_discontinued'
        };
        getProductsCount();
        $scope.getProductsFromPage(1);
    };
});

app.controller('ProductFormController', function ($scope, $state, $stateParams, Product) {
    /**
     * Initialize controller.
     * @public
     */
    $scope.init = function () {
        if ($stateParams.id) {
            $scope.mode = 'Editing';
            $scope.product = Product.get({id:$stateParams.id});
        } else {
            $scope.mode = 'Creating';
            $scope.product = new Product();
        }
    };

    var onSuccess = function () {
        alert("Done. Redirect to homepage.");
        $state.go('products');
    };

    var onError = function ($response) {
        $scope.errors = $response.data;
    };
    /**
     * Create or update product entity.
     * @public
     */
    $scope.submitProduct = function () {
        if ($stateParams.id) {
            $scope.product.$update(onSuccess, onError)
        } else
        {
            $scope.product.$create(onSuccess, onError)
        }
    }
});

app.controller('SearchController', function ($scope, EntitySelector) {

    /**
     * Init search controller
     */
    $scope.init = function () {
        $scope.filters = [];
        $scope.fieldNames = {
            'Code': 'str_product_code',
            'Name': 'str_product_name',
            'Description': 'str_product_desc',
            'Stock': 'int_stock',
            'Cost in GBP': 'float_price'
        };
        $scope.options = {
            'equals': '=',
            'greater than': '>',
            'less than': '<',
            'contains': 'like'
        };
    };

    /**
     * Add filter to filters array
     * @public
     */
    $scope.addFilter = function () {
        $scope.filters.push({});
    };

    /**
     * Delete filter from filters array
     * @param $index
     * @public
     */
    $scope.deleteFilter = function ($index) {
      $scope.filters.splice($index, 1);
    };

    /**
     * Filter data according to filter array.
     * @public
     */
    $scope.filterData = function () {
        EntitySelector.clearFilters();
        $scope.filters.forEach(function (filter) {
            if (filter.option === 'like') {
                EntitySelector.addFilter(
                    filter.field + ' ' + filter.option + ' ' + "'%" + filter.value +"%'"
                );
            } else {
                EntitySelector.addFilter(
                    filter.field + ' ' + filter.option +  ' ' + filter.value
                );
            }
        });
        $scope.$parent.init();
    }
});
<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 22.07.2016
 * Time: 13:10
 */

namespace AppBundle\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('strProductCode', TextType::class);
        $builder->add('strProductName', TextType::class);
        $builder->add('strProductDesc', TextType::class);
        $builder->add('intStock', IntegerType::class);
        $builder->add('floatPrice', NumberType::class);
        $builder->add('dtmDiscontinued', DateTimeType::class, array(
            'input' => 'datetime',
            'widget' => 'single_text',
            'format' => "yyyy-MM-dd'T'HH:mm"
        ));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'product';
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 04.07.2016
 * Time: 10:39
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportDataFromFileCommand extends ContainerAwareCommand
{
    /**
     * ImportDataFromFileCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Configure new console command
     */
    protected function configure()
    {
        $this
            ->setName('appBundle:database:import')
            ->setDescription('Import data from csv file to database.')
            ->addArgument(
                'filename',
                InputOption::VALUE_REQUIRED,
                'Path and name of your csv file.'
            )
            ->addOption(
                'test',
                't',
                InputOption::VALUE_OPTIONAL,
                'Execute operation without database update.'
            );
    }

    /**
     * Execute console command operation.
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $styleOutput = new SymfonyStyle($input, $output);
        $styleOutput->setVerbosity(OutputInterface::VERBOSITY_NORMAL);
        $filename = $input->getArgument('filename');
        $importDataHelper = $this->getContainer()->get('app.import_helper');
        $logger = new ConsoleLogger($styleOutput);
        $styleOutput->title('Import');
        $importDataHelper->import($filename, $input->getOption('test'), $logger);
        $styleOutput->success('Import finished');
    }
}

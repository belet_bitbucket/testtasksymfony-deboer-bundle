<?php

/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 18.07.2016
 * Time: 17:48
 */
namespace AppBundle\Classes\ImportHelper\Writer;

use Ddeboer\DataImport\Writer\DoctrineWriter;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class StreamDoctrineWriter extends DoctrineWriter
{
    /** @var  int */
    private $objectsPerOneFlush;

    /** @var LoggerInterface  */
    private $logger;

    /** @var  int */
    private $counter;

    /** @var ValidatorInterface */
    private $validator;

    /**
     * @return int
     */
    public function getObjectsPerOneFlush()
    {
        return $this->objectsPerOneFlush;
    }

    /**
     * @param int $objectsPerOneFlush
     */
    public function setObjectsPerOneFlush($objectsPerOneFlush)
    {
        $this->objectsPerOneFlush = $objectsPerOneFlush;
    }

    /**
     * StreamDoctrineWriter constructor.
     * @param EntityManagerInterface $entityManager
     * @param string $entityName
     * @param ValidatorInterface $entityValidator
     * @param LoggerInterface $logger
     * @param int $objectsPerOneFlush
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        $entityName,
        ValidatorInterface $entityValidator,
        LoggerInterface $logger,
        $objectsPerOneFlush = 5
    ) {
        parent::__construct($entityManager, $entityName);
        $this->logger = $logger;
        $this->counter = 0;
        $this->validator = $entityValidator;
        $this->objectsPerOneFlush = $objectsPerOneFlush;
    }

    /**
     * Validate and persist item. Flush item every $objectsPerOneFlush items.
     * @param array $item
     * @return $this
     */
    public function writeItem(array $item)
    {
        $entity = $this->findOrCreateItem($item);
        $this->loadAssociationObjectsToEntity($item, $entity);
        $this->updateEntity($item, $entity);
        $errors = $this->validator->validate($entity, null, ['import']);
        if (count($errors) !== 0) {
            if ($item['dtmDiscontinued']) {
                $item['dtmDiscontinued'] = 'yes';
            }
            foreach ($errors as $error) {
                $this->logger->alert(sprintf(
                    'Record %s will not be added: %s',
                    implode(',', $item),
                    $error->getMessage()
                ));
            }
        } else {
            $this->counter++;
            $this->entityManager->persist($entity);

            if (($this->counter % $this->objectsPerOneFlush) == 0) {
                $this->flush();
            }
        }
        return $this;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 19.07.2016
 * Time: 11:14
 */

namespace AppBundle\Classes\ImportHelper;

use Ddeboer\DataImport\Step;
use Symfony\Component\Form\Exception\RuntimeException;

class CheckMd5Step implements Step
{
    /** @var string  */
    private $md5;

    /** @var string  */
    private $filename;

    /**
     * CheckMd5Step constructor.
     * @param string $filename
     */
    public function __construct($filename)
    {
        $this->md5 = md5_file($filename);
        $this->filename = $filename;
    }

    /**
     * Any processing done on each item in the data stack
     *
     * @param mixed &$item
     *
     * @return boolean False return value means the item should be skipped
     */
    public function process(&$item)
    {
        if ($this->md5 === md5_file($this->filename)) {
            return true;
        }
        throw new RuntimeException('FILE WAS CHANGED DURING IMPORT. ROLLBACK ALL CHANGES.');
    }
}

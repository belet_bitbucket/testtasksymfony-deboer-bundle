<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 04.07.2016
 * Time: 14:48
 */

namespace AppBundle\Classes\ImportHelper;

use Exception;

class InputRecordValidator
{
    /** @var string  */
    private $error = '';

    /** @var array */
    private $fieldNames = [
        'strProductCode',
        'strProductName',
        'strProductDesc',
        'intStock',
        'floatPrice',
        'dtmDiscontinued'
    ];

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Check is input row valid. If not push error message to input $error array.
     * @param array $row
     *
     * @return bool
     */
    public function isRecordValid($row)
    {
        try {
            $this->checkIsEachFieldSet($row);
            $this->checkProductCode($row['strProductCode']);
            $this->checkStockValue($row['intStock']);
            $this->checkPriceValue($row['floatPrice']);
            $this->checkDiscontinuedValue($row['dtmDiscontinued']);
            $this->checkLimits($row['floatPrice'], $row['intStock']);
        } catch (Exception $e) {
            $this->error = $e->getMessage();
            return false;
        }
        return true;
    }

    /**
     * Check is product code valid. If not throw exception.
     * @param string $value
     * @throws Exception
     */
    private function checkProductCode($value)
    {
        if (preg_match('#P\\d\\d\\d\\d#', $value) === 0 || strlen($value) !== 5) {
            throw new Exception('Product code format is incorrect');
        }
    }

    /**
     * Check is stock value valid. If not throw exception.
     * @param string $value
     * @throws Exception
     */
    private function checkStockValue($value)
    {
        if (!ctype_digit($value)) {
            throw new Exception('In stock should be positive integer');
        }
    }

    /**
     * Check is price valid. If not throw exception.
     * @param string $value
     * @throws Exception
     */
    private function checkPriceValue($value)
    {
        if (!is_numeric($value)) {
            throw new Exception('Price should be numeric type');
        }
    }

    /**
     * Check is Discontinued param value valid. If not throw exception.
     * @param string $value
     * @throws Exception
     */
    private function checkDiscontinuedValue($value)
    {
        if ($value!=='yes' && $value!=='') {
            throw new Exception('Discontinued may have only values "yes" and empty value');
        }
    }

    /**
     * Check limits on price and stock values. If values invalid throw exception.
     * @param $price
     * @param $stock
     * @throws Exception
     */
    private function checkLimits($price, $stock)
    {
        if ($price>1000) {
            throw new Exception('Price greater than $1000');
        } if ($price < 5 && $stock < 10) {
            throw new Exception('Price less than $5 and stock less than 10');
        }
    }

    /**
     * Check is each field in row has value
     * @param $row
     * @throws Exception
     */
    private function checkIsEachFieldSet($row)
    {
        foreach ($this->fieldNames as $fieldName) {
            if (!isset($row[$fieldName])) {
                throw new Exception(sprintf('Field %s is unset', $fieldName));
            }
        }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 05.07.2016
 * Time: 17:09
 */

namespace AppBundle\Classes\ImportHelper;

use AppBundle\Classes\ImportHelper\Writer\StreamDoctrineWriter;
use AppBundle\Entity\Product;
use Ddeboer\DataImport\Reader\CsvReader;
use Ddeboer\DataImport\Step\FilterStep;
use Ddeboer\DataImport\Step\ValueConverterStep;
use Ddeboer\DataImport\Workflow\StepAggregator;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ImportDataHelper
{
    
    /** @var \Doctrine\ORM\EntityManager  */
    protected $entityManager;
    
    /** @var ValidatorInterface  */
    protected $entityValidator;

    /** @var  LoggerInterface */
    private $logger;

    /** @var string  */
    private $autoDetectLineEndingsValue;

    /**
     * ImportDataHelper constructor.
     * @param EntityManager $entityManager
     * @param ValidatorInterface $entityValidator
     */
    public function __construct(
        EntityManager $entityManager,
        ValidatorInterface $entityValidator
    ) {
        $this->entityManager = $entityManager;
        $this->entityValidator = $entityValidator;
        $this->autoDetectLineEndingsValue = ini_get('auto_detect_line_endings');
        ini_set('auto_detect_line_endings', '1');
    }

    /**
     * ImportDataHelper destructor.
     * Set auto_detect_line_endings to default value.
     */
    public function __destruct()
    {
        ini_set('auto_detect_line_endings', $this->autoDetectLineEndingsValue);
    }

    /**
     * Import data from CsvReader to database
     * @param string $filename
     * @param $inTestMode
     * @param LoggerInterface $logger
     */
    public function import($filename, $inTestMode, LoggerInterface $logger = null)
    {
        $reader = $this->prepareReader($filename);
        $workflow = new StepAggregator($reader);
        if ($logger) {
            $workflow->setLogger($logger);
            $this->logger = $logger;
        }
        $this->entityManager->beginTransaction();
        $writer = new StreamDoctrineWriter(
            $this->entityManager,
            Product::class,
            $this->entityValidator,
            $this->logger
        );
        $writer->setTruncate(false);
        $workflow->addWriter($writer)
            ->setSkipItemOnFailure(true)
            ->addStep($this->getValidateRecordStep())
            ->addStep($this->getDiscontinuedValueConvertStep())
            ->addStep(new CheckMd5Step($filename));
        $workflow->process();
        if ($inTestMode) {
            $this->entityManager->rollback();
        } else {
            $this->entityManager->commit();
        }
    }

    /**
     * Return CSV reader.
     * @param $filename
     * @return CsvReader
     */
    private function prepareReader($filename)
    {
        $reader = new CsvReader(new \SplFileObject($filename));
        $reader->setHeaderRowNumber(0);
        $headerMapper = array(
            'Product Code' => 'strProductCode',
            'Product Name' => 'strProductName',
            'Product Description' => 'strProductDesc',
            'Stock' => 'intStock',
            'Cost in GBP' => 'floatPrice',
            'Discontinued' => 'dtmDiscontinued'
        );
        $newHeader = array();
        foreach ($reader->getColumnHeaders() as $columnHeader) {
            $newHeader[] = $headerMapper[$columnHeader];
        }
        $reader->setColumnHeaders($newHeader);
        return $reader;
    }

    /**
     * @return ValueConverterStep object
     */
    private function getDiscontinuedValueConvertStep()
    {
        $callable = function ($discontinued) {
            return $discontinued === 'yes' ? new \DateTime(): null;
        };
        $valueConverter = new ValueConverterStep();
        $valueConverter->add('[dtmDiscontinued]', $callable)
            ->add('[intStock]', function ($value) {
                return intval($value);
            })
            ->add('[floatPrice]', function ($value) {
                return floatval($value);
            });
        return $valueConverter;
    }

    /**
     * @return FilterStep object
     */
    private function getValidateRecordStep()
    {
        $filter = (new FilterStep())
            ->add([$this, 'checkCsvData']);
        return $filter;
    }

    /**
     * Check is record valid.
     * @param array $record
     * @return bool
     */
    public function checkCsvData($record)
    {
        if ($record) {
            $validator = new InputRecordValidator();
            $result = $validator->isRecordValid($record);
            if (!$result) {
                $this->logger->alert(sprintf(
                    'Record %s will not be added: %s',
                    implode(',', $record),
                    $validator->getError()
                ));
                return false;
            }
            return true;
        }
        return false;
    }
}

<?php

/**
 * Created by PhpStorm.
 * User: a2.beletsky
 * Date: 29.07.2016
 * Time: 15:22
 */
namespace AppBundle\Classes\Deserializers;

class ProductsRequestPropertiesDeserializer
{
    /**
     * Deserialize filter string for use in DQl.
     * @param string $filter
     * @return string
     */
    public function deserializeFilter($filter)
    {
        $words = explode(' ', $filter);
        $words[0] = $this->toCamelCase($words[0]);
        $words[1] = strtoupper($words[1]);
        return implode(' ', $words);
    }

    /**
     * Deserialize fieldname in order object received from front-end.
     * @param \stdClass $order
     * @return \stdClass
     */
    public function deserializeOrder($order)
    {
        $order->field = $this->toCamelCase($order->field);
        return $order;
    }

    private function toCamelCase($value)
    {
        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $value))));
    }
}
